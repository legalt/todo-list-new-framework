/**
 * Main application entry point.
 */

'use strict';

var app = require('spa-app');


// everything is ready
app.once('load', function () {
    // load pages
    app.pages = {
        main: require('./pages/main')
    };

    app.route(app.pages.main);
});
