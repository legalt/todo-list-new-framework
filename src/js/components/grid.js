/**
 * @module stb/ui/grid
 * @author Stanislav Kalashnik <sk@infomir.eu>
 * @license GNU GENERAL PUBLIC LICENSE Version 3
 */

'use strict';

var StbGrid = require('spa-component-grid'),
    Input   = require('spa-component-input'),
    CheckBox   = require('spa-component-checkbox');

/**
 * Mouse click event.
 *
 * @event module:stb/ui/grid~Grid#click:item
 *
 * @type {Object}
 * @property {Element} $item clicked HTML item
 * @property {Event} event click event data
 */


/**
 * Base grid/table implementation.
 *
 * For navigation map implementation and tests see {@link https://gist.github.com/DarkPark/8c0c2926bfa234043ed1}.
 *
 * Each data cell can be either a primitive value or an object with these fields:
 *
 *  Name    | Description
 * ---------|-------------
 *  value   | actual cell value to render
 *  colSpan | amount of cells to merge horizontally
 *  rowSpan | amount of cells to merge vertically
 *  mark    | is it necessary or not to render this cell as marked
 *  focus   | is it necessary or not to render this cell as focused
 *  disable | is it necessary or not to set this cell as disabled
 *
 * @constructor
 * @extends Component
 *
 * @param {Object}   [config={}] init parameters (all inherited from the parent)
 * @param {Array[]}  [config.data=[]] component data to visualize
 * @param {function} [config.render] method to build each grid cell content
 * @param {function} [config.navigate] method to move focus according to pressed keys
 * @param {boolean}  [config.cycleX=true] allow or not to jump to the opposite side of line when there is nowhere to go next
 * @param {boolean}  [config.cycleY=true] allow or not to jump to the opposite side of column when there is nowhere to go next
 * @param {object}   [config.provider] data provider
 * @param {number}   [config.sizeX] grid columns count
 * @param {number}   [config.sizeX] grid rows count
 *
 * @fires module:stb/ui/grid~Grid#click:item
 *
 * @example
 * var Grid = require('stb/ui/grid'),
 *     grid = new Grid({
 *         data: [
 *             [1,   2,  3, {value: '4;8;12;16', focus: true, rowSpan: 4}],
 *             [5,   6,  7],
 *             [9,  10, 11],
 *             [13, 14, {value: 15, disable: true}]
 *         ],
 *         render: function ( $item, data ) {
 *             $item.innerHTML = '<div>' + (data.value) + '</div>';
 *         },
 *         cycleX: false
 *     });
 */
function Grid ( config ) {

    this.$tbody = null;

    this.$thead = null;

    /**
     * Selected items
     *
     * @type {Array}
     */
    this.selected = [];

    // set default className if classList property empty or undefined
    config.className = 'complex ' + (config.className || '');

    // parent constructor call
    StbGrid.call(this, config);

    // component setup
    this.init(config);
}


// inheritance
Grid.prototype = Object.create(StbGrid.prototype);
Grid.prototype.constructor = Grid;

Grid.prototype.defaultEvents = {
    /**
     * Default method to handle mouse wheel events.
     *
     * @param {Event} event generated event
     */
    mousewheel: function ( event ) {
    },

    /**
     * Default method to handle keyboard keydown events.
     *
     * @param {Event} event generated event
     */
    keydown: function ( event ) {

    }
};


/**
 * Make all the data items identical.
 * Wrap to objects if necessary and add missing properties.
 *
 * @param {Array[]} data user 2-dimensional array
 * @return {Array[]} reworked incoming data
 */
function normalize ( data ) {
    var i, j, item;

    if ( DEBUG ) {
        if ( arguments.length !== 1 ) { throw new Error(__filename + ': wrong arguments number'); }
        if ( !Array.isArray(data) ) { throw new Error(__filename + ': wrong data type'); }
    }

    // rows
    for ( i = 0; i < data.length; i++ ) {
        // cols
        for ( j = 0; j < data[i].length; j++ ) {
            // cell value
            item = data[i][j];
            // primitive value
            if ( typeof item !== 'object' ) {
                // wrap with defaults
                item = data[i][j] = {
                    value: data[i][j],
                    colSpan: 1,
                    rowSpan: 1
                };
            } else {
                // always at least one row/col
                item.colSpan = item.colSpan || 1;
                item.rowSpan = item.rowSpan || 1;
            }

            if ( DEBUG ) {
                //if ( !('value' in item) ) { throw new Error(__filename + ': field "value" is missing'); }
                if ( Number(item.colSpan) !== item.colSpan ) { throw new Error(__filename + ': item.colSpan must be a number'); }
                if ( Number(item.rowSpan) !== item.rowSpan ) { throw new Error(__filename + ': item.rowSpan must be a number'); }
                if ( item.colSpan <= 0 ) { throw new Error(__filename + ': item.colSpan should be positive'); }
                if ( item.rowSpan <= 0 ) { throw new Error(__filename + ': item.rowSpan should be positive'); }
                if ( ('focus' in item) && Boolean(item.focus) !== item.focus ) { throw new Error(__filename + ': item.focus must be boolean'); }
                if ( ('disable' in item) && Boolean(item.disable) !== item.disable ) { throw new Error(__filename + ': item.disable must be boolean'); }
            }
        }
    }

    return data;
}


/**
 * Fill the given rectangle area with value.
 *
 * @param {Array[]} map link to navigation map
 * @param {number} x current horizontal position
 * @param {number} y current vertical position
 * @param {number} dX amount of horizontal cell to fill
 * @param {number} dY amount of vertical cell to fill
 * @param {*} value filling data
 */
function fill ( map, x, y, dX, dY, value ) {
    var i, j;

    if ( DEBUG ) {
        if ( arguments.length !== 6 ) { throw new Error(__filename + ': wrong arguments number'); }
        if ( !Array.isArray(map) ) { throw new Error(__filename + ': wrong map type'); }
    }

    // rows
    for ( i = y; i < y + dY; i++ ) {
        // expand map rows
        if ( map.length < i + 1 ) { map.push([]); }

        // compensate long columns from previous rows
        while ( map[i][x] !== undefined ) {
            x++;
        }

        // cols
        for ( j = x; j < x + dX; j++ ) {
            // expand map row cols
            if ( map[i].length < j + 1 ) { map[i].push(); }
            // fill
            map[i][j] = value;
            // apply coordinates for future mouse clicks
            if ( value.x === undefined ) { value.x = j; }
            if ( value.y === undefined ) { value.y = i; }
        }
    }
}


/**
 * Create a navigation map from incoming data.
 *
 * @param {Array[]} data user 2-dimensional array of objects
 * @return {Array[]} navigation map
 */
function map ( data ) {
    var result = [],
        i, j, item;

    if ( DEBUG ) {
        if ( arguments.length !== 1 ) { throw new Error(__filename + ': wrong arguments number'); }
        if ( !Array.isArray(data) ) { throw new Error(__filename + ': wrong data type'); }
    }

    // rows
    for ( i = 0; i < data.length; i++ ) {
        // cols
        for ( j = 0; j < data[i].length; j++ ) {
            // cell value
            item = data[i][j];
            // process a cell
            fill(result, j, i, item.colSpan, item.rowSpan, item.$item);
            // clear redundant info
            delete item.$item;
        }
    }

    return result;
}


Grid.prototype.renderHeadItem = function ( $item, data ) {
    var self = this;

    if ( data.className ) {
        $item.className = 'item ' + data.className;
    }

    if ( data.type === 'dropdown' ) {
        $item.$dropdownIcon = $item.appendChild(document.createElement('div'));
        $item.$dropdownIcon.className = 'dropdown icon-angle-right';

    } else if ( data.type === 'checkbox' ) {
        $item.parentNode.checkbox = new CheckBox();

        $item.parentNode.checkbox.addListener('change', function ( event ) {
            var i = 0,
                size = self.$tbody.childElementCount;

            if ( !event.value ) {
                this.$node.classList.remove('icon-minus');
            }

            while ( i < size ) {
                if ( self.$tbody.rows[i].checkbox ) {
                    self.$tbody.rows[i].checkbox.set(event.value);
                }
                ++i;
            }
            self.selected.sort(function (a, b) {
                return a - b;
            });
        });

        $item.appendChild($item.parentNode.checkbox.$node);

    } else if ( data.type === 'input' ) {
        $item.$table = $item.appendChild(document.createElement('div'));
        $item.$table.className = 'table';
        $item.appendChild($item.$table);

        $item.$filterIcon = $item.$table.appendChild(document.createElement('span'));
        $item.$filterIcon.className = 'filter';
        $item.$filterIcon.innerHTML = '<span class="icon-filter"></span>';

        $item.input = new Input({
            type: Input.prototype.TYPE_SEARCH,
            $node: document.createElement('div'),
            $body: document.createElement('input'),
            visible: false,
            events: {
                show: function () {
                    this.$body.removeAttribute('readonly');
                    this.$body.placeholder = 'filter';
                },
                hide: function () {
                    this.$body.setAttribute('readonly', 'readonly');
                    this.$body.placeholder = '';
                }
            }
        });
        $item.input.$node.appendChild($item.input.$body);
        $item.input.$node.clasName = 'input';
        $item.$table.appendChild($item.input.$node);


        $item.$span = $item.appendChild(document.createElement('span'));
        $item.$span.className = 'text';
        $item.$span.textContent = data.value;
    } else {
        $item.textContent = data.value;
    }
};


/**
 * Init or re-init of the component inner structures and HTML.
 *
 * @param {Object} config init parameters (subset of constructor config params)
 */
Grid.prototype.init = function ( config ) {
    var self = this,
        draw = false,
        i, j,
        $row, $item, $focusItem,
        itemData,
        /**
         * Cell mouse click handler.
         *
         * @param {Event} event click event data
         *
         * @this Element
         *
         * @fires module:stb/ui/grid~Grid#click:item
         */
        onItemClick = function ( event ) {
            // allow to accept focus
            if ( this.data.disable !== true ) {
                // visualize
                self.focusItem(this);

                // there are some listeners
                if ( self.events['click:item'] ) {
                    // notify listeners
                    self.emit('click:item', {$item: this, event: event});
                }
            }
        },
        /**
         * Header cell mouse click handler.
         *
         * @param {Event} event click event data
         *
         * @this Element
         *
         * @fires module:stb/ui/grid~Grid#click:item
         */
        onHeaderItemClick = function () {
            if ( this.$filterIcon ) {
                if ( this.input.visible ) {
                    this.input.hide();
                    this.input.$body.blur();
                } else {
                    this.input.show();
                    this.input.$body.focus();
                }
            }

            // there are some listeners
            if ( self.events['header:item:click'] ) {
                // notify listeners
                self.emit('header:item:click', {$item: this});
            }
        },
        /**
         * Construct grid when receive new data
         *
         * @param {Array} data to render
         */
        construct = function ( data ) {

            // apply data
            if ( data ) {
                // new data is different
                if ( self.data !== data ) {
                    // apply
                    self.data = data;
                    // need to redraw table
                    draw = true;
                }
            }

            // custom render method
            if ( config.render ) {
                // new render is different
                if ( self.renderItem !== config.render ) {
                    // apply
                    self.renderItem = config.render;
                    // need to redraw table
                    draw = true;
                }
            }

            // custom render method
            if ( config.headerRender ) {
                // new render is different
                if ( self.renderHeadItem !== config.headerRender ) {
                    // apply
                    self.renderHeadItem = config.headerRender;
                    // need to redraw table
                    draw = true;
                }
            }

            if ( !draw ) {
                // do not redraw table
                return;
            }

            // export pointer to inner table
            self.$table = document.createElement('table');
            self.$tbody = document.createElement('tbody');
            self.$thead = document.createElement('thead');

            if ( config.header ) {
                self.$thead = document.createElement('thead');
                // prepare user data
                self.headerData = normalize(config.header);

                // rows
                for ( i = 0; i < self.headerData.length; i++ ) {
                    // dom
                    $row = self.$thead.insertRow();

                    // cols
                    for ( j = 0; j < self.headerData[i].length; j++ ) {
                        // dom
                        $item = $row.insertCell(-1);
                        // additional params
                        $item.className = 'item';

                        // shortcut
                        itemData = self.headerData[i][j];

                        // for map
                        itemData.$item = $item;

                        // merge columns
                        $item.colSpan = itemData.colSpan;

                        // merge rows
                        $item.rowSpan = itemData.rowSpan;

                        // visualize
                        self.renderHeadItem($item, itemData);
//                        self.renderItem($item, itemData);



                        // save data link
                        $item.data = itemData;

                        // manual focusing
                        $item.addEventListener('click', onHeaderItemClick);
                    }
                    // row is ready
                    self.$thead.appendChild($row);
                }
            }

            // prepare user data
            self.data = normalize(self.data);

            // rows
            for ( i = 0; i < self.data.length; i++ ) {
                // dom
                $row = self.$tbody.insertRow();

                // cols
                for ( j = 0; j < self.data[i].length; j++ ) {
                    // dom
                    $item = $row.insertCell(-1);
                    // additional params
                    $item.className = 'item';

                    // shortcut
                    itemData = self.data[i][j];

                    // for map
                    itemData.$item = $item;

                    // merge columns
                    $item.colSpan = itemData.colSpan;

                    // merge rows
                    $item.rowSpan = itemData.rowSpan;

                    // active cell
                    if ( itemData.focus ) {
                        // store and clean
                        $focusItem = $item;
                    }

                    // disabled cell
                    if ( itemData.disable ) {
                        // apply CSS
                        $item.classList.add('disable');
                    }

                    // marked cell
                    if ( itemData.mark ) {
                        // apply CSS
                        $item.classList.add('mark');
                    }

                    // visualize
                    self.renderItem($item, itemData);

                    // save data link
                    $item.data = itemData;

                    // manual focusing
                    $item.addEventListener('click', onItemClick);
                }
                // row is ready
                self.$tbody.appendChild($row);
            }

            // navigation map filling
            self.map = map(self.data);

            // clear all table
            self.$body.textContent = null;

            // everything is ready
            self.$table.appendChild(self.$thead);
            self.$table.appendChild(self.$tbody);
            self.$body.appendChild(self.$table);

            // apply focus
            if ( $focusItem ) {
                // focus item was given in data
                self.focusItem($focusItem);
            } else if ( self.map[0] ) {
                // just the first cell
                self.focusItem(self.map[0][0]);
            }
        };

    if ( DEBUG ) {
        if ( arguments.length !== 1 ) { throw new Error(__filename + ': wrong arguments number'); }
        if ( typeof config !== 'object' ) { throw new Error(__filename + ': wrong config type'); }
        if ( config.data && (!Array.isArray(config.data) || !Array.isArray(config.data[0])) ) { throw new Error(__filename + ': wrong config.data type'); }
        if ( config.render && typeof config.render !== 'function' ) { throw new Error(__filename + ': wrong config.render type'); }
    }

    // apply cycle behaviour
    if ( config.cycleX !== undefined ) {
        this.cycleX = config.cycleX;
    }
    if ( config.cycleY !== undefined ) {
        this.cycleY = config.cycleY;
    }

    construct(config.data);
};


/**
 * Filters row if a certain column data correspond to the filter.
 *
 *
 * @param {number} columnIndex.table column index
 * @param {number} $item.y the item vertical position
 *
 * @return {boolean} operation status
 */
Grid.prototype.filterColumn = function ( columnIndex, filterCallback ) {
    var $row, rowIndex, size, index;

    if ( DEBUG ) {
        if ( arguments.length > 2 ) { throw new Error(__filename + ': wrong arguments number'); }
    }

    if ( typeof filterCallback === 'function' ) {
        rowIndex = 0;
        size = this.$tbody.rows.length;

        while ( rowIndex < size ) {
            $row = this.$tbody.rows[rowIndex];
            if ( $row.children[columnIndex] ) {
                if ( !$row.filtered ) {
                    $row.filtered = [];
                }

                index = $row.filtered.indexOf(columnIndex);

                if ( filterCallback($row.children[columnIndex].data, rowIndex) ) {
                    $row.classList.add('hidden');
                    if ( index === -1 ) {
                        $row.filtered.push(columnIndex);
                    }
                } else if ( index !== -1 ) {
                    $row.filtered.splice(index, 1)
                }
                if ( $row.filtered.length === 0 ) {
                    $row.classList.remove('hidden');
                }
            }

            ++rowIndex;
        }
    } else {
        // reset column data
    }
};


/**
 * Highlight the given DOM element as focused.
 * Remove focus from the previously focused item.
 *
 * @param {Node|Element} $item element to focus
 * @param {number} $item.x the item horizontal position
 * @param {number} $item.y the item vertical position
 *
 * @return {boolean} operation status
 *
 * @fires module:stb/ui/grid~Grid#focus:item
 * @fires module:stb/ui/grid~Grid#blur:item
 */
Grid.prototype.focusItem = function ( $item ) {
    var $prev = this.$focusItem;

    if ( DEBUG ) {
        if ( arguments.length !== 1 ) { throw new Error(__filename + ': wrong arguments number'); }
    }

    // different element
    if ( $item && $prev !== $item && $item.data.disable !== true ) {
        if ( DEBUG ) {
            if ( !($item instanceof Element) ) { throw new Error(__filename + ': wrong $item type'); }
            if ( $item.parentNode.parentNode.parentNode.parentNode !== this.$body ) { throw new Error(__filename + ': wrong $item parent element'); }
        }

        // some item is focused already
        if ( $prev !== null ) {
            if ( DEBUG ) {
                if ( !($prev instanceof Element) ) { throw new Error(__filename + ': wrong $prev type'); }
            }

            // style
            $prev.classList.remove('focus');

            // there are some listeners
            if ( this.events['blur:item'] ) {
                /**
                 * Remove focus from an element.
                 *
                 * @event module:stb/ui/grid~Grid#blur:item
                 *
                 * @type {Object}
                 * @property {Element} $item previously focused HTML element
                 */
                this.emit('blur:item', {$item: $prev});
            }
        }

        // draft coordinates
        this.focusX = $item.x;
        this.focusY = $item.y;

        // reassign
        this.$focusItem = $item;

        // correct CSS
        $item.classList.add('focus');

        // there are some listeners
        if ( this.events['focus:item'] ) {
            /**
             * Set focus to an element.
             *
             * @event module:stb/ui/grid~Grid#focus:item
             *
             * @type {Object}
             * @property {Element} $prev old/previous focused HTML element
             * @property {Element} $curr new/current focused HTML element
             */
            this.emit('focus:item', {$prev: $prev, $curr: $item});
        }

        return true;
    }

    // nothing was done
    return false;
};


/**
 * Set item state and appearance as marked.
 *
 * @param {Node|Element} $item element to focus
 * @param {boolean} state true - marked, false - not marked
 */
Grid.prototype.markItem = function ( $item, state ) {
    if ( DEBUG ) {
        if ( arguments.length !== 2 ) { throw new Error(__filename + ': wrong arguments number'); }
        if ( !($item instanceof Element) ) { throw new Error(__filename + ': wrong $item type'); }
        if ( $item.parentNode.parentNode.parentNode.parentNode !== this.$body ) { throw new Error(__filename + ': wrong $item parent element'); }
        if ( Boolean(state) !== state ) { throw new Error(__filename + ': state must be boolean'); }
    }

    // correct CSS
    if ( state ) {
        $item.classList.add('mark');
    } else {
        $item.classList.remove('mark');
    }

    // apply flag
    $item.data.mark = state;
};


/**
 * Insert row into specific index.
 *
 * @param {Object} config init parameters (subset of constructor config params)
 */
Grid.prototype.addRow = function ( data, index ) {
    var self = this,
        $row = this.$tbody.insertRow(index || -1),
        onClick = function ( event ) {
            // allow to accept focus
            if ( this.data.disable !== true ) {
                // visualize
                self.focusItem(this);

                // there are some listeners
                if ( self.events['click:item'] ) {
                    // notify listeners
                    self.emit('click:item', {$item: this, event: event});
                }
            }
        },
        cellIndex = 0,
        cellSize = data.length,
        $cell;

    this.data.push(data);

    while ( cellIndex < cellSize ) {
        $cell = $row.insertCell(-1);
        $cell.addEventListener('click', onClick);
        $cell.data = data[cellIndex];
        this.renderItem($cell, $cell.data);

        ++cellIndex;
    }
};


/**
 * Insert row into specific index.
 *
 * @param {Object} config init parameters (subset of constructor config params)
 */
Grid.prototype.removeRow = function ( $row ) {
    this.data.splice($row.rowIndex, 1);

    this.$tbody.removeChild($row);
};


/**
 * Insert row into specific index.
 *
 * @param {Object} config init parameters (subset of constructor config params)
 */
Grid.prototype.clearBody = function () {
    this.data = [];
    while ( this.$tbody.lastChild ) {
        this.$tbody.removeChild(this.$tbody.lastChild);
    }
};


if ( DEBUG ) {
    // expose to the global scope
    window.ComponentMyGrid = Grid;
}


// public
module.exports = Grid;
