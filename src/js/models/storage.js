/**
 * Created by legalt on 01.09.16.
 */
'use strict';

function Storage ( name ) {
    this.name = name;
}

Storage.prototype.constructor = Storage;

Storage.prototype.setItem = function ( data ) {
    localStorage.setItem(this.name, JSON.stringify(data));
};

Storage.prototype.getItem = function () {
    return JSON.parse(localStorage.getItem(this.name));
};

module.exports = Storage;