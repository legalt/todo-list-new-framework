/**
 * Created by legalt on 30.08.16.
 */

'use strict';

var Storage = require('./storage');


function Task () {
    var STORAGE_NAME = 'tasks';

    this.STATUSES = {
        OPEN: 0,
        COMPLETED: 1
    };

    Storage.call(this, STORAGE_NAME);

    this.tasks = this.getItem() || [];
}

//inherit
Task.prototype = Storage.prototype;
Task.prototype.constructor = Task;


Task.prototype.add = function ( title ) {
    var newTask = {
        id: Date.now(),
        title: title,
        status: this.STATUSES.OPEN
    };

    this.tasks.push(newTask);
    this.setItem(this.tasks);

    return newTask;
};

Task.prototype.update = function ( id, title, status ) {
    this.tasks = this.tasks.map(function ( item ) {
        if ( item.id === id ) {
            if ( title ) {
                item.title = title;
            }

            if ( status >= 0 ) {
                item.status = status;
            }
        }

        return item;
    });

    this.setItem(this.tasks);
};

Task.prototype.remove = function ( id ) {
    this.tasks = this.tasks.filter(function ( item ) {
        return item.id !== id;
    });

    this.setItem(this.tasks);
};

Task.prototype.list = function ( status ) {
    if ( status >= 0 ) {
        return this.tasks.filter(function ( item ) {
            return item.status === status;
        });
    }

    return this.tasks;
};

Task.prototype.getCount = function ( status ) {
    var tasks = this.tasks.filter(function ( item ) {
        if ( status === undefined ) {
            return true;
        }

        return item.status === status;
    });

    return tasks.length;
};

module.exports = Task;
