/**
 * Main page implementation.
 */

'use strict';

var Page = require('spa-component-page'),
    page = new Page({$node: window.pageMain}),
    Grid = require('../components/grid'),
    Button = require('spa-component-button'),
    Checkbox = require('spa-component-checkbox'),
    Input = require('spa-component-input'),

    Task = require('./../models/task'),
    task = new Task();


function fillList ( status ) {
    var list = task.list(status),
        rowData, index;

    page.taskList.clearBody();

    for ( index = 0; index < list.length; index++ ) {
        rowData = [
            {
                task: list[index],
                className: 'toggle',
                value: list[index].status,
                type: 'checkbox'
            },
            {
                task: list[index],
                className: 'title',
                value: list[index].title
            },
            {
                task: list[index],
                className: 'actions',
                type: 'buttons'
            }
        ];
        page.taskList.addRow(rowData);
    }

    if ( status === undefined ) {
        if ( list.length ) {
            window.taskControl.className = '';
        } else {
            window.taskControl.className = 'hidden';
        }
    }
}

page.taskList = new Grid({
    $node: window.taskList,
    render: function ( $item, data ) {
        $item.rowTitle;
        switch ( data.type ) {
            case 'buttons' :
                $item.removeItem = new Button({
                    value: '',
                    className: 'btn-remove',
                    title: 'Remove item',
                    focusable: false,
                    events: {
                        click: function ( ) {
                            page.taskList.removeRow($item.parentNode);
                            task.remove(data.task.id);

                            if ( !page.taskList.$tbody.childNodes.length ) {
                                window.taskControl.className = 'hidden';
                            } else {
                                window.totalOpen.innerText = task.getCount(task.STATUSES.OPEN);
                            }
                        }
                    }
                });
                $item.appendChild($item.removeItem.$node);
                break;

            case 'checkbox':
                $item.toogleItem = new Checkbox({
                    className: data.className,
                    value: data.value,
                    focusable: false,
                    events: {
                        click: function () {
                            console.log($item)
                            if ( data.task.status ) {
                                $item.toogleItem.set(task.STATUSES.OPEN);
                                data.task.status = task.STATUSES.OPEN;
                                task.update(data.task.id, null, task.STATUSES.OPEN);
                                rowTitle.className = rowTitle.className.replace('completed', '').trim();
                            } else {
                                $item.toogleItem.set(task.STATUSES.COMPLETED);
                                data.task.status = task.STATUSES.COMPLETED;
                                task.update(data.task.id, null, task.STATUSES.COMPLETED);
                                rowTitle.className = rowTitle.className + ' completed';
                            }

                            window.totalOpen.innerText = task.getCount(task.STATUSES.OPEN);
                        }
                    }
                });
                $item.appendChild($item.toogleItem.$node);
                break;
            default:
                if ( data.task.status ) {
                    $item.className = data.className + ' completed';
                } else {
                    $item.className = data.className;
                }

                $item.innerHTML = data.value;
                $item.rowTitle = $item;
                break;
        }
    }
});

page.taskTitle = new Input({
    $node: window.taskTitle,
    placeholder: 'What needs to be done ?',
    events: {
        keypress: function ( event ) {
            var title, rowData, newTask;

            if ( event.keyCode === 13 ) {
                title = this.value.trim();

                if ( title ) {
                    newTask = task.add(title);
                    window.taskTitle.value = '';

                    rowData = [
                        {
                            task: newTask,
                            className: 'toggle',
                            type: 'checkbox',
                            value: newTask.status
                        },
                        {
                            task: newTask,
                            className: 'title',
                            value: newTask.title
                        },
                        {
                            task: newTask,
                            className: 'actions',
                            type: 'buttons'
                        }
                    ];

                    page.taskList.addRow(rowData);
                    window.taskControl.className = '';
                    window.totalOpen.innerText = task.getCount(task.STATUSES.OPEN);
                }
            }
        }
    }
});

page.once('show', function () {
    fillList();

    window.totalOpen.innerText = task.getCount(task.STATUSES.OPEN);
});

page.add(new Checkbox({
    $node: window.fAll,
    focusable: false,
    events: {
        click: function () {
            fillList();
        }
    }
}));

page.add(new Checkbox({
    $node: window.fOpen,
    focusable: false,
    events: {
        click: function () {
            fillList(task.STATUSES.OPEN);
        }
    }
}));

page.add(new Checkbox({
    $node: window.fCompleted,
    focusable: false,
    events: {
        click: function () {
            fillList(task.STATUSES.COMPLETED);
        }
    }
}));

// public
module.exports = page;
